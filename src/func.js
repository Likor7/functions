const {listOfPosts} = require("./posts");
const getSum = (str1, str2) => {
  if(typeof str1 !== 'string'
      || typeof str2 !== 'string'
      || str1.replace(/\D/g, '').length !== str1.length
      || str2.replace(/\D/g, '').length !== str2.length)
  {
    return false;
  }

  if(str1 === '')
  {
    str1=0;
  }
  if( str2 === '')
  {
   str2=0;
  }

  return (parseInt(str1)+parseInt(str2)).toString();
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let postCount = listOfPosts.filter(x=> x.author === authorName).length;
  let counter = [];
  for(let elementList of listOfPosts)
  {
    if(elementList.hasOwnProperty("comments"))
    {
      for(let element of elementList.comments)
      {
        counter.push(element);
      }
    }
  }
  let commentCount = counter.filter(x=> x.author === authorName).length;
  return 'Post:'+postCount+',comments:'+commentCount;
};
getQuantityPostsByAuthor(listOfPosts, 'Rimus')

const tickets=(people)=> {
  people = people.map(x => parseInt(x))
  let sum = 0;
  for(let i = 0; i < people.length; i++)
  {
    if(people[i] === 25 )
    {
      sum+=25
    }
    if(people[i] !== 25 && i === 0)
    {
      return 'NO'
    }
    if(people[i]!==25)
    {
      if(sum >= people[i]-25)
      {
        sum += 25;
      }
      else
      {
        return 'NO';
      }
    }
  }
  return 'YES';
};

module.exports = {getSum, getQuantityPostsByAuthor, tickets};
